package com.coatalpol.api;

import com.coatalpol.repository.AdminRepo;
import com.coatalpol.object.Admin;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

public class AdminA extends Jooby {
	{
		/**
	     *
	     * Everything about Admin.
	     */
	    path("/api/admin", () -> {
	      /**
	       *
	       * List Admin ordered by id.
	       *
	       * @param start Start offset, useful for paging. Default is <code>0</code>.
	       * @param max Max page size, useful for paging. Default is <code>50</code>.
	       * @return Marker ordered by id.
	       */
	      get(req -> {
	        AdminRepo db = require(AdminRepo.class);

	        int start = req.param("start").intValue(0);
	        int max = req.param("max").intValue(20);

	        return db.list(start, max);
	      });

	      /**
	       *
	       * List Admin ordered by id.
	       *
	       * @param start Start offset, useful for paging. Default is <code>0</code>.
	       * @param max Max page size, useful for paging. Default is <code>50</code>.
	       * @return Admin ordered by id_admin.
	       */
	      post(req -> {
	        // MarkerRepo db = require(MarkerRepo.class);

	        // int start = req.param("start").intValue(0);
	        // int max = req.param("max").intValue(20);

	      	Admin cekData = req.body(Admin.class);
	      	AdminRepo db = require(AdminRepo.class);
	      	int id = db.insert(cekData);
	        return id;
	      });
	  });
	}
}
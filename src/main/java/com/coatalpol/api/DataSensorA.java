package com.coatalpol.api;

import com.coatalpol.repository.DataSensorRepo;
import com.coatalpol.object.DataSensor;

import java.net.URL;
import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

public class DataSensorA extends Jooby {

	{
		get("/api/datasensorAll", req -> {
    		DataSensorRepo db = require(DataSensorRepo.class);

    		return db.listAll();
    	});

		/**
	     *
	     * Everything about Data Sensor.
	     */
	    path("/api/datasensor", () -> {
	      /**
	       *
	       * List Data Sensor ordered by id.
	       *
	       * @param start Start offset, useful for paging. Default is <code>0</code>.
	       * @param max Max page size, useful for paging. Default is <code>50</code>.
	       * @return Marker ordered by id.
	       */
	      get(req -> {
	        DataSensorRepo db = require(DataSensorRepo.class);

	        int hal = req.param("hal").intValue();

	        if (hal < 1) {
	        	hal = 1;
	        }

	        int start = (hal-1)*10;
	        int max = 10;

	        return db.list(start, max);
	      });

	      get("/:id", req -> {
	      	DataSensorRepo db = require(DataSensorRepo.class);

	      	int id = req.param("id").intValue();
	      	DataSensor datasensor = db.findById(id);

	      	if (datasensor == null) {
	      		throw new Err(Status.NOT_FOUND);
	      	}
	      	return datasensor;
	      });

	      /**
	       *
	       * List Data Sensor ordered by id.
	       *
	       * @param start Start offset, useful for paging. Default is <code>0</code>.
	       * @param max Max page size, useful for paging. Default is <code>50</code>.
	       * @return Marker ordered by geom.
	       */
	      post(req -> {
	        // MarkerRepo db = require(MarkerRepo.class);

	        // int start = req.param("start").intValue(0);
	        // int max = req.param("max").intValue(20);

	      	DataSensor cekData = req.body(DataSensor.class);
	      	DataSensorRepo db = require(DataSensorRepo.class);
	      	int id = db.insert(cekData);

	        return id;
	      });

	      /**
		 * Update Features berdasarkan ID.
		 */
		put(req -> {
			DataSensorRepo db = require(DataSensorRepo.class);
			DataSensor datasensor = req.body(DataSensor.class);
			boolean status = false;
	      	System.out.println(new URL("http://localhost:8181/api/hitungmetode?idSensor="+datasensor.getIdSensor()).openStream());
			
			if (!db.update(datasensor)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}
			
			return status;
		});

		/**
		 * Delete Features berdasarkan ID.
		 */
		delete("/:id", req -> {
			DataSensorRepo db = require(DataSensorRepo.class);
			int id = req.param("id").intValue();
			boolean status = false;

			if (!db.delete(id)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}

			return status;
		});
	  });

	    path("/api/max-hal/datasensor", () -> {
	    	/**
	    	* Mendapatkan halaman maks Data Sensor
	    	*/
	    	get(req -> {
	    		DataSensorRepo db = require(DataSensorRepo.class);
	        	double jmlTotal;

	        	jmlTotal = (double) db.findTot();
	        	// } else {
	        	// 	jmlTotal = (double) db.findTot(hal);
	        	// }

	        	double halTotal = jmlTotal/10;
	        	if ((halTotal%1) > 0) {
	        		halTotal = halTotal - (halTotal%1) + 1;
	        	}

	        	return (int) halTotal;
	    	});
	    });

	    path("/api/jml-data/datasensor", () -> {
	    	/**
	    	* Mendapatkan halaman maks Data Sensor
	    	*/
	    	get(req -> {
	    		DataSensorRepo db = require(DataSensorRepo.class);
	        	
	        	return db.findTot();
	    	});
	    });

	    path("/api/jml-stasiun/datasensor", () -> {
	    	/**
	    	* Mendapatkan jumlah stasiun dari data sensor
	    	*/
	    	get(req -> {
	    		DataSensorRepo db = require(DataSensorRepo.class);
	        	
	        	return db.totalStasiun();
	    	});
	    });

	    path("/api/tampil-waktu/datasensor", () -> {
	    	/**
	    	* Mendapatkan jumlah stasiun dari data sensor
	    	*/
	    	get(req -> {
	    		DataSensorRepo db = require(DataSensorRepo.class);
	        	
	        	return db.showTime();
	    	});
	    });
	}
}
package com.coatalpol.api;

import com.coatalpol.method.*;
import com.coatalpol.object.DataSensor;
import com.coatalpol.repository.DataSensorRepo;
import com.coatalpol.object.HitungMetode;
import com.coatalpol.repository.HitungMetodeRepo;
import com.coatalpol.api.DataSensorA;

import java.math.*;
import java.text.DecimalFormat;
import java.util.List;
import java.text.DecimalFormat;
import java.util.ArrayList;

import java.util.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

public class HitungMetodeA extends Jooby {
	{
		get("/api/hitungmetodeAll", req -> {
    		HitungMetodeRepo db = require(HitungMetodeRepo.class);

    		return db.listAll();
    	});

    	get("/api/rata-rata/hitungmetode", req -> {
    		HitungMetodeRepo db = require(HitungMetodeRepo.class);
			int bulan = req.param("bulan").intValue(1);

    		return db.rata(bulan);
    	});

    	get("/api/status-indeks/hitungmetode", req -> {
    		HitungMetodeRepo db = require(HitungMetodeRepo.class);
    		DataSensorRepo ds = require(DataSensorRepo.class);

    		List<HitungMetode> toReturn = new ArrayList<>();
    		List<DataSensor> getAll = ds.listAll();

    		int bulan = req.param("bulan").intValue();

    		for (int i=0; i < getAll.size(); i++) {
    			toReturn.add(db.findForGraph(bulan, getAll.get(i).getIdSensor()));
    			System.out.println(i + ": " + getAll.get(i).getNama());
    		}

    		return toReturn;
    	});

		path("/api/hitungmetode", () -> {
			get(req -> {
				DataSensorRepo db = require(DataSensorRepo.class);
				HitungMetodeRepo hm = require(HitungMetodeRepo.class);
				List<DataSensor> dataSensorList = db.listAll();

				int idSensor = req.param("idSensor").intValue(0);

				ArrayList <ArrayList <Double>> var= new ArrayList <ArrayList <Double>>();
		        ArrayList<Double> max = new ArrayList<Double>();
		        ArrayList<Double> rata2 = new ArrayList<Double>();
		        ArrayList<Double> index = new ArrayList<Double>();
		        ArrayList<String> status = new ArrayList<String>();

        		ArrayList<Double> ph = new ArrayList<Double>(); 
        		ArrayList<Double> disox = new ArrayList<Double>();
        		ArrayList<Double> sal = new ArrayList<Double>();
        		ArrayList<Double> suhu = new ArrayList<Double>();
        		ArrayList<Double> cerah = new ArrayList<Double>();

        		DecimalFormat df = new DecimalFormat("#.###");

        		// menambahkan data ke var
        		for (int i = 0; i < dataSensorList.size(); i++) {

        			ph.add(Double.parseDouble(dataSensorList.get(i).getNilaiPh()));
        			disox.add(Double.parseDouble(dataSensorList.get(i).getNilaiDo()));
        			sal.add(Double.parseDouble(dataSensorList.get(i).getNilaiSal()));
        			suhu.add(Double.parseDouble(dataSensorList.get(i).getNilaiSuhu()));
        			cerah.add(Double.parseDouble(dataSensorList.get(i).getNilaiCerah()));
        		}

        		var.add(ph);
		        var.add(disox);
		        var.add(sal);
		        var.add(suhu);
		        var.add(cerah);
		        var.add(max);
		        var.add(rata2);        
		        var.add(index);

        		//mencari nilai maks tiap baris
        		for (int i = 0; i < ph.size(); i++) {
        			System.out.println("Stasiun: " + dataSensorList.get(i).getNama());
		            HitungPH hitungPh = new HitungPH((double) ph.get(i));
		            HitungDO hitungDo = new HitungDO((double) disox.get(i));
		            HitungSal hitungSal = new HitungSal((double) sal.get(i));
		            HitungSuhu hitungSuhu = new HitungSuhu((double) suhu.get(i));
		            HitungCerah hitungCerah = new HitungCerah((double) cerah.get(i));

		            double maks = hitungPh.getPhBaru();
		            if (maks < hitungDo.getDoBaru()) {
		            	maks = hitungDo.getDoBaru();
		            } else if (maks < hitungSal.getSalBaru()) {
		            	maks = hitungSal.getSalBaru();
		            } else if (maks < hitungSuhu.getSuhuBaru()) {
		            	maks = hitungSuhu.getSuhuBaru();
		            } else {
		            	maks = hitungCerah.getCerahBaru();
		            }
		            max.add(maks);
		        }

		        for (int i = 0; i < ph.size(); i++) {
		        	double sum, rata;

		        	HitungPH hitungPh = new HitungPH((double) ph.get(i));
		            HitungDO hitungDo = new HitungDO((double) disox.get(i));
		            HitungSal hitungSal = new HitungSal((double) sal.get(i));
		            HitungSuhu hitungSuhu = new HitungSuhu((double) suhu.get(i));
		            HitungCerah hitungCerah = new HitungCerah((double) cerah.get(i));
		        	
		        	sum = hitungPh.getPhBaru() + hitungDo.getDoBaru() + hitungSal.getSalBaru() 
		        		+ hitungSuhu.getSuhuBaru() + hitungCerah.getCerahBaru();
		        	rata = sum / 5;
		        	rata2.add(rata);
		        }

		        for (int i = 0; i < ph.size(); i++) {
		        	double indeks = Math.sqrt((Math.pow((double) max.get(i), 2)+ Math.pow((double) rata2.get(i), 2))/2);

		        	index.add(indeks);
		        }

		        for (int i = 0; i < ph.size(); i++) {
		        	double ip = (double) index.get(i);
		        	if (ip <= 0 && ip <= 1) {
						status.add("MBM");
		        	} else if(ip > 1 && ip <=5) {
						status.add("TR");
            		} else if(ip > 5 && ip <= 10) {
						status.add("TS");
            		} else {
						status.add("TT");
            		}
		        }

		        // post hasil
		        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		        Timestamp ts = new Timestamp(System.currentTimeMillis());

		        if (idSensor == 0) {
		        	for (int i = 0; i < dataSensorList.size(); i++) {
		        		HitungMetode hitmed = new HitungMetode(1, dataSensorList.get(i).getIdSensor(), index.get(i), status.get(i), sdf.format(ts));

		        		hm.insert(hitmed);
		        	}	
		        } else {
		        	double indexInput = 0;
		        	String statusInput = "";

		        	for(int i=0; i<dataSensorList.size(); i++) {
		        		if(dataSensorList.get(i).getIdSensor() == idSensor) {
		        			indexInput = index.get(i);
		        			statusInput = status.get(i);
		        		}
		        	}

		        	HitungMetode hitmed = new HitungMetode(1, idSensor, indexInput, statusInput, sdf.format(ts));

		        	hm.insert(hitmed);
		        	// System.out.println(idSensor);
		        }
		        
				return idSensor;
			});
		});

		path("/api/tampil-waktu/hitungmetode", () ->{
			get(req -> {
				HitungMetodeRepo db = require(HitungMetodeRepo.class);

				return db.showTime();
			});
		});

	}
}
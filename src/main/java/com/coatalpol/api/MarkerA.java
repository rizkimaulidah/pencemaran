package com.coatalpol.api;

import com.coatalpol.repository.MarkerRepo;
import com.coatalpol.object.Marker;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

public class MarkerA extends Jooby {
	{
		/**
	     *
	     * Everything about Marker.
	     */
	    path("/api/marker", () -> {
	      /**
	       *
	       * List Marker ordered by id.
	       *
	       * @param start Start offset, useful for paging. Default is <code>0</code>.
	       * @param max Max page size, useful for paging. Default is <code>50</code>.
	       * @return Marker ordered by id.
	       */
	      get(req -> {
	        MarkerRepo db = require(MarkerRepo.class);

	        int start = req.param("start").intValue(0);
	        int max = req.param("max").intValue(20);

	        return db.list(start, max);
	      });

	      /**
	       *
	       * List Marker ordered by id.
	       *
	       * @param start Start offset, useful for paging. Default is <code>0</code>.
	       * @param max Max page size, useful for paging. Default is <code>50</code>.
	       * @return Marker ordered by geom.
	       */
	      post(req -> {
	        // MarkerRepo db = require(MarkerRepo.class);

	        // int start = req.param("start").intValue(0);
	        // int max = req.param("max").intValue(20);

	      	Marker cekData = req.body(Marker.class);
	      	MarkerRepo db = require(MarkerRepo.class);
	      	int id = db.insert(cekData);
	        return id;
	      });
	  });
	}
}
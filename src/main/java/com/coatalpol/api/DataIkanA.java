package com.coatalpol.api;

import com.coatalpol.repository.DataIkanRepo;
import com.coatalpol.object.DataIkan;

import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;

public class DataIkanA extends Jooby {
	{
		get("/api/dataikanAll", req -> {
    		DataIkanRepo db = require(DataIkanRepo.class);

    		return db.listAll();
    	});

    	get("/api/total-ikan-sehat/dataikan", req -> {
    		DataIkanRepo db = require(DataIkanRepo.class);
			int bulan = req.param("bulan").intValue(1);

    		return db.totalIkanSehat(bulan);
    	});

    	get("/api/total-ikan-sakit/dataikan", req -> {
    		DataIkanRepo db = require(DataIkanRepo.class);
			int bulan = req.param("bulan").intValue(1);

    		return db.totalIkanSakit(bulan);
    	});

    	get("/api/total-desc/dataikan", req -> {
    		DataIkanRepo db = require(DataIkanRepo.class);
			int bulan = req.param("bulan").intValue(1);

    		return db.totalDesc(bulan);
    	});

		/**
	     *
	     * Everything about Data Ikan.
	     */
	    path("/api/dataikan", () -> {
	      /**
	       *
	       * List Data Ikan ordered by id.
	       *
	       * @param start Start offset, useful for paging. Default is <code>0</code>.
	       * @param max Max page size, useful for paging. Default is <code>50</code>.
	       * @return Marker ordered by id.
	       */
	      get(req -> {
	        DataIkanRepo db = require(DataIkanRepo.class);

	        int halaman = req.param("halaman").intValue();

	        if (halaman < 1) {
	        	halaman = 1;
	        }

	        int start = (halaman-1)*10;
	        int max = 10;

	        return db.list(start, max);
	      });

	      get("/:id", req -> {
	      	DataIkanRepo db = require(DataIkanRepo.class);

	      	int id = req.param("id").intValue();
	      	DataIkan dataikan = db.findById(id);

	      	if (dataikan == null) {
	      		throw new Err(Status.NOT_FOUND);
	      	}
	      	return dataikan;
	      });

	      /**
	       *
	       * List Data Ikan ordered by id.
	       *
	       * @param start Start offset, useful for paging. Default is <code>0</code>.
	       * @param max Max page size, useful for paging. Default is <code>50</code>.
	       * @return Marker ordered by geom.
	       */
	      post(req -> {
	        // MarkerRepo db = require(MarkerRepo.class);

	        // int start = req.param("start").intValue(0);
	        // int max = req.param("max").intValue(20);

	      	DataIkan cekData = req.body(DataIkan.class);
	      	DataIkanRepo db = require(DataIkanRepo.class);
	      	int id = db.insert(cekData);
	        return id;
	      });

	      /**
		 * Update Features berdasarkan ID.
		 */
		put(req -> {
			DataIkanRepo db = require(DataIkanRepo.class);
			DataIkan dataikan = req.body(DataIkan.class);
			boolean status = false;
			
			if (!db.update(dataikan)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}
			
			return status;
		});

		/**
		 * Delete Features berdasarkan ID.
		 */
		delete("/:id", req -> {
			DataIkanRepo db = require(DataIkanRepo.class);
			int id = req.param("id").intValue();
			boolean status = false;

			if (!db.delete(id)) {
				throw new Err(Status.NOT_FOUND);
			} else {
				status = true;
			}

			return status;
		});
	  });

	    path("/api/max-hal/dataikan", () -> {
	    	/**
	    	* Mendapatkan halaman maks Data Sensor
	    	*/
	    	get(req -> {
	    		DataIkanRepo db = require(DataIkanRepo.class);
	        	double jmlTotal;

	        	jmlTotal = (double) db.total();

	        	double halTotal = jmlTotal/10;
	        	if ((halTotal%1) > 0) {
	        		halTotal = halTotal - (halTotal%1) + 1;
	        	}

	        	return (int) halTotal;
	    	});
	    });
	}
}
package com.coatalpol.repository;

import com.coatalpol.object.Admin;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Database access for Marker.
 */
@RegisterRowMapper(Admin.Mapper.class)
public interface AdminRepo {
      /**
   * List Admin using start/max limits.
   *
   * @param start Start offset.
   * @param max Max number of rows.
   * @return Available Marker.
   */
  @SqlQuery("SELECT id_admin, username, passwd, role_admin FROM tbl_admin LIMIT :max OFFSET :start")
  List<Admin> list(int start, int max);

  /**
   * Find a Admin by ID.
   *
   * @param id Admin ID.
   * @return Admin or null.
   */
  @SqlQuery("SELECT id_admin, username, passwd, role_admin FROM tbl_admin WHERE id=:gid")
  Admin findById(int id_admin);

   /**
   * Insert a Admin and returns the generated PK.
   *
   * @param Admin Admin to insert.
   * @return Primary key.
   */
   @SqlUpdate("INSERT INTO tbl_admin(username, passwd, role_admin) values(:username, :passwd, :role_admin)")
   @GetGeneratedKeys
   int insert(@BindBean Admin Admin);

  //~ /**
   //~ * Update a Marker and returns true if the Markers was updated.
   //~ *
   //~ * @param Marker Marker to update.
   //~ * @return True if the Marker was updated.
   //~ */
  //~ @SqlUpdate("update Markers set name=:name where id=:id")
  //~ boolean update(@BindBean Marker Marker);

  //~ /**
   //~ * Delete a Marker by ID.
   //~ *
   //~ * @param id Marker ID.
   //~ * @return True if the Marker was deleted.
   //~ */
  //~ @SqlUpdate("delete Markers where id=:id")
  //~ boolean delete(long id);
}

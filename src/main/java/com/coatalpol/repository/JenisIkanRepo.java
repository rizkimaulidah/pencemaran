package com.coatalpol.repository;

import com.coatalpol.object.JenisIkan;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel tbl_jenis_ikan.
 */
@RegisterRowMapper(JenisIkan.Mapper.class)
public interface JenisIkanRepo {
  /**
   * Daftar jenis ikan dengan jumlah max data dan dimulai dari start
   */
  @SqlQuery("SELECT id_jenis, jenis_ikan FROM tbl_jenis_ikan LIMIT :max OFFSET :start")
  List<JenisIkan> list(int start, int max);

  /**
   * Daftar jenis ikan berdasarka id
   */
  @SqlQuery("SELECT * FROM tbl_jenis_ikan WHERE id=:id_jenis")
  JenisIkan findById(int id_ikan);

   /**
   * Menambahkan data pada tbl_jenis_ikan
   */
   @SqlUpdate("INSERT INTO tbl_jenis_ikan(jenis_ikan) VALUES(:jenisIkan)")
   @GetGeneratedKeys
   int insert(@BindBean JenisIkan JenisIkan);

  /**
  * Mengupdate tbl_jenis_ikan berdasarkan id dan return keberhasilan update
  */
    @SqlUpdate("UPDATE tbl_jenis_ikan SET jenis_ikan=:jenis_ikan WHERE id=:id_jenis")
    boolean update(@BindBean JenisIkan JenisIkan);

  //~ /**
   //~ * Delete a Data Ikan by ID.
   //~ *
   //~ * @param id Data Ikan ID.
   //~ * @return True if the Data Ikan was deleted.
   //~ */
    @SqlUpdate("DELETE tbl_jenis_ikan WHERE id=:id_jenis")
    boolean delete(long id);
}

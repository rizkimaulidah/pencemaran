package com.coatalpol.repository;

import com.coatalpol.object.MarkerLokasi;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Database access for KantorPolisis.
 */
@RegisterRowMapper(MarkerLokasi.Mapper.class)
public interface MarkerLokasiRepo {
  /**
   * List KantorPolisi using start/max limits.
   *
   * @param start Start offset.
   * @param max Max number of rows.
   * @return Available KantorPolisi.
   */
  @SqlQuery("SELECT gid, name, latitude, longitude, ST_AsText(the_geom), lokasi FROM marker LIMIT :max OFFSET :start")
  List<MarkerLokasi> list(int start, int max);

  /**
   * Find a KantorPolisi by ID.
   *
   * @param id KantorPolisi ID.
   * @return KantorPolisi or null.
   */
  @SqlQuery("SELECT gid, name, latitude, longitude, ST_AsText(the_geom), lokasi FROM marker WHERE id=:gid")
  MarkerLokasi findById(int id);

  //~ /**
   //~ * Insert a KantorPolisi and returns the generated PK.
   //~ *
   //~ * @param KantorPolisi KantorPolisi to insert.
   //~ * @return Primary key.
   //~ */
  //~ @SqlUpdate("insert into KantorPolisis(name) values(:name)")
  //~ @GetGeneratedKeys
  //~ long insert(@BindBean KantorPolisi KantorPolisi);

  //~ /**
   //~ * Update a KantorPolisi and returns true if the KantorPolisis was updated.
   //~ *
   //~ * @param KantorPolisi KantorPolisi to update.
   //~ * @return True if the KantorPolisi was updated.
   //~ */
  //~ @SqlUpdate("update KantorPolisis set name=:name where id=:id")
  //~ boolean update(@BindBean KantorPolisi KantorPolisi);

  //~ /**
   //~ * Delete a KantorPolisi by ID.
   //~ *
   //~ * @param id KantorPolisi ID.
   //~ * @return True if the KantorPolisi was deleted.
   //~ */
  //~ @SqlUpdate("delete KantorPolisis where id=:id")
  //~ boolean delete(long id);
}

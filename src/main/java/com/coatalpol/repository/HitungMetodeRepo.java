package com.coatalpol.repository;

import com.coatalpol.object.HitungMetode;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Database access for Hitung Metode.
 */
@RegisterRowMapper(HitungMetode.Mapper.class)
public interface HitungMetodeRepo {
	@SqlQuery("SELECT id, id_data_sensor, indeks, status, waktu_input FROM tbl_parameter LIMIT :max OFFSET :start")
  	List<HitungMetode> list(int start, int max);

	//list hasil hitung metode tampil semua
	@SqlQuery("SELECT id, id_data_sensor, indeks, status, waktu_input FROM tbl_parameter ORDER BY waktu_input DESC")
	List<HitungMetode> listAll();

	/**
	  * Get total item dari datang hitung metode
	*/
	@SqlQuery("SELECT COUNT(*) FROM tbl_parameter")
  	int findTot();

  	/**
	  * Get total stasiun dari datang hitung metode
	*/
  	@SqlQuery("SELECT COUNT(*) FROM ( SELECT COUNT(*) FROM tbl_parameter) AS sub;")
  	int totalIndeks();
    
    /**
    * Get average from hitung metode
    */
    @SqlQuery("SELECT AVG(indeks) FROM tbl_parameter WHERE extract(month from waktu_input)=:bulan")
    double rata(int bulan);

  	/**
   * Find a Data Hitung Metode by ID.
   *
   * @param id ID.
   * @return Indeks or null.
   */
  	@SqlQuery("SELECT id, id_data_sensor, indeks, status, waktu_input FROM tbl_parameter WHERE id=:id")
  	HitungMetode findById(int id);
    /**
   * show all time by ID
   *
   * @param id Sensor ID.
   * @return Data Sensor or null.
   */
  @SqlQuery("SELECT DISTINCT waktu_input::date FROM tbl_parameter")
  List<String> showTime();

  /**
   * show status dan indeks
   *
   * @param id Sensor ID.
   * @return Data Sensor or null.
   */
  @SqlQuery("SELECT * FROM tbl_parameter WHERE EXTRACT(MONTH from waktu_input)=:bulan AND id_data_sensor=:idDataSensor")
  HitungMetode findForGraph(int bulan, int idDataSensor);

  	/**
   	* Insert an indeks and returns the generated PK.
   	*
   	* @param Hitung Metode to insert.
   	* @return Primary key.
   	*/
   	@SqlUpdate("INSERT INTO tbl_parameter(id_data_sensor, indeks, status, waktu_input) VALUES(:idDataSensor, :indeks, :status, to_timestamp(:waktuInput, 'YYYY-MM-DD HH24:MI:MS')) ")
   	@GetGeneratedKeys
   	int insert(@BindBean HitungMetode HitungMetode);

	//~ /**
	//~ * Delete a Marker by ID.
	//~ *
	//~ * @param id Marker ID.
	//~ * @return True if the Marker was deleted.
	//~ */
   	@SqlUpdate("DELETE FROM tbl_parameter WHERE id=:id")
   	boolean delete(long id);
}

package com.coatalpol.repository;

import com.coatalpol.object.Marker;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Database access for Marker.
 */
@RegisterRowMapper(Marker.Mapper.class)
public interface MarkerRepo {
  /**
   * List Marker using start/max limits.
   *
   * @param start Start offset.
   * @param max Max number of rows.
   * @return Available Marker.
   */
  @SqlQuery("SELECT gid, name, ST_AsText(the_geom) FROM marker LIMIT :max OFFSET :start")
  List<Marker> list(int start, int max);

  /**
   * Find a Marker by ID.
   *
   * @param id Marker ID.
   * @return Marker or null.
   */
  @SqlQuery("SELECT gid, name, ST_AsText(the_geom) FROM marker WHERE id=:gid")
  Marker findById(int gid);

   /**
   * Insert a Marker and returns the generated PK.
   *
   * @param Marker Marker to insert.
   * @return Primary key.
   */
   @SqlUpdate("INSERT INTO marker(name, the_geom) values(:name, ST_GeomFromText(:lokasi, 4326))")
   @GetGeneratedKeys
   int insert(@BindBean Marker Marker);

  //~ /**
   //~ * Update a Marker and returns true if the Markers was updated.
   //~ *
   //~ * @param Marker Marker to update.
   //~ * @return True if the Marker was updated.
   //~ */
  //~ @SqlUpdate("update Markers set name=:name where id=:id")
  //~ boolean update(@BindBean Marker Marker);

  //~ /**
   //~ * Delete a Marker by ID.
   //~ *
   //~ * @param id Marker ID.
   //~ * @return True if the Marker was deleted.
   //~ */
  //~ @SqlUpdate("delete Markers where id=:id")
  //~ boolean delete(long id);
}

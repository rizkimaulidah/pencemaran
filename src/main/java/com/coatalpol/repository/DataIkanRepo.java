package com.coatalpol.repository;

import com.coatalpol.object.DataIkan;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Akses database untuk tabel tbl_data_ikan.
 */
@RegisterRowMapper(DataIkan.Mapper.class)
public interface DataIkanRepo {
  /**
   * Daftar jenis ikan dengan jumlah max data dan dimulai dari start
   */
  @SqlQuery("SELECT id_ikan, jenis_ikan, jml_ikan_sehat, jml_ikan_sakit, wkt_input FROM tbl_data_ikan ORDER BY id_ikan desc LIMIT :max OFFSET :start")
  List<DataIkan> list(int start, int max);

  /**
   * Daftar jenis ikan berdasarka id
   */
  @SqlQuery("SELECT id_ikan, jenis_ikan, jml_ikan_sehat, jml_ikan_sakit, wkt_input FROM tbl_data_ikan WHERE id_ikan=:id_ikan")
  DataIkan findById(int id_ikan);

  // list Data Sensor tampil semua
  @SqlQuery("SELECT id_ikan, jenis_ikan, jml_ikan_sehat, jml_ikan_sakit, wkt_input FROM tbl_data_ikan ORDER BY id_ikan desc")
  List<DataIkan> listAll();

  /**
   * Get total item dari data ikan
   */
  @SqlQuery("SELECT COUNT(*) FROM tbl_data_ikan")
  int total();

  /**
   * Get total ikan sehat dari data ikan
   */
  @SqlQuery("SELECT SUM(jml_ikan_sehat::numeric) FROM tbl_data_ikan WHERE EXTRACT(MONTH FROM wkt_input)=:bulan")
  int totalIkanSehat(int bulan);

    /**
   * Get total ikan sakit dari data ikan
   */
  @SqlQuery("SELECT SUM(jml_ikan_sakit::numeric) FROM tbl_data_ikan WHERE EXTRACT(MONTH FROM wkt_input)=:bulan")
  int totalIkanSakit(int bulan);

    /**
   * Get desc total ikan sehat dari data ikan
   */
  @SqlQuery("SELECT * FROM tbl_data_ikan WHERE EXTRACT(MONTH FROM wkt_input)=:bulan ORDER BY jml_ikan_sehat DESC")
  List<DataIkan> totalDesc(int bulan);
  /**
   * Menambahkan data pada tbl_data_ikan
   */
  @SqlUpdate("INSERT INTO tbl_data_ikan(jenis_ikan, jml_ikan_sehat, jml_ikan_sakit, wkt_input) VALUES(:jenisIkan, :jmlIkanSehat, :jmlIkanSakit, to_timestamp(:wktInput, 'YYYY-MM-DD HH24:MI:MS'))")
  @GetGeneratedKeys
  int insert(@BindBean DataIkan DataIkan);

  /**
  * Mengupdate tbl_data_ikan berdasarkan id dan return keberhasilan update
  */
  @SqlUpdate("UPDATE tbl_data_ikan SET jenis_ikan=:jenisIkan, jml_ikan_sehat=:jmlIkanSehat, jml_ikan_sakit=:jmlIkanSakit, wkt_input=to_timestamp(:wktInput, 'YYYY-MM-DD HH24:MI:MS') WHERE id_ikan=:idIkan")
  boolean update(@BindBean DataIkan DataIkan);

  //~ /**
   //~ * Delete a Data Ikan by ID.
   //~ *
   //~ * @param id Data Ikan ID.
   //~ * @return True if the Data Ikan was deleted.
   //~ */
  @SqlUpdate("DELETE FROM tbl_data_ikan WHERE id_ikan=:id")
  boolean delete(int id);
}

package com.coatalpol.repository;

import com.coatalpol.object.DataSensor;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

/**
 * Database access for Data Sensor.
 */
@RegisterRowMapper(DataSensor.Mapper.class)
public interface DataSensorRepo {
     /**
   * List Data Sensor using start/max limits.
   *
   * @param start Start offset.
   * @param max Max number of rows.
   * @return Available Data Sensor.
   */
  @SqlQuery("SELECT id_sensor, nilai_ph, nilai_do, nilai_cerah, nilai_sal, nilai_suhu, wkt_input, nama, lokasi, ST_AsText(geom) FROM tbl_data_sensor ORDER BY overlay(nama placing '' from 1 for 1)::int asc LIMIT :max OFFSET :start")
  List<DataSensor> list(int start, int max);
  
  // list Data Sensor tampil semua
  @SqlQuery("SELECT id_sensor, nilai_ph, nilai_do, nilai_cerah, nilai_sal, nilai_suhu, wkt_input, nama, lokasi, ST_AsText(geom) FROM tbl_data_sensor ORDER BY overlay(nama placing '' from 1 for 1)::int asc")
  List<DataSensor> listAll();

  /**
  * Get total item dari data sensor
  */
  @SqlQuery("SELECT COUNT(*) FROM tbl_data_sensor")
  int findTot();

  /**
  * Get total stasiun dari data sensor
  */
  @SqlQuery("SELECT COUNT(*) FROM ( SELECT COUNT(*) FROM tbl_data_sensor GROUP BY nama) AS sub;")
  int totalStasiun();

  /**
   * Find a Data Sensor by ID.
   *
   * @param id Sensor ID.
   * @return Data Sensor or null.
   */
  @SqlQuery("SELECT id_sensor, nilai_ph, nilai_do, nilai_cerah, nilai_sal, nilai_suhu, wkt_input, nama, lokasi, ST_AsText(geom) FROM tbl_data_sensor WHERE id_sensor=:id_sensor")
  DataSensor findById(int id_sensor);
  /**
   * show all time by ID
   *
   * @param id Sensor ID.
   * @return Data Sensor or null.
   */
  @SqlQuery("SELECT DISTINCT wkt_input::date FROM tbl_data_sensor")
  List<String> showTime();

   /**
   * Insert a Data Sensor and returns the generated PK.
   *
   * @param Data Sensor Data Sensor to insert.
   * @return Primary key.
   */
   @SqlUpdate("INSERT INTO tbl_data_sensor(nilai_ph, nilai_do, nilai_cerah, nilai_sal, nilai_suhu, wkt_input, nama, lokasi, geom) VALUES(:nilaiPh, :nilaiDo, :nilaiCerah, :nilaiSal, :nilaiSuhu, to_timestamp(:wktInput, 'YYYY-MM-DD HH24:MI:MS'), :nama, :lokasi, ST_GeomFromText(:geom, 4326))")
   @GetGeneratedKeys
   int insert(@BindBean DataSensor DataSensor);

  //~ /**
   //~ * Update a Marker and returns true if the Markers was updated.
   //~ *
   //~ * @param Marker Marker to update.
   //~ * @return True if the Marker was updated.
   //~ */
   @SqlUpdate("UPDATE tbl_data_sensor SET nilai_ph=:nilaiPh, nilai_do=:nilaiDo, nilai_cerah=:nilaiCerah, nilai_sal=:nilaiSal, nilai_suhu=:nilaiSuhu, wkt_input=to_timestamp(:wktInput, 'YYYY-MM-DD HH24:MI:MS'), nama=:nama, lokasi=:lokasi, geom=ST_GeomFromText(:geom, 4326) where id_sensor=:idSensor")
   boolean update(@BindBean DataSensor DataSensor);

  //~ /**
   //~ * Delete a Marker by ID.
   //~ *
   //~ * @param id Marker ID.
   //~ * @return True if the Marker was deleted.
   //~ */
   @SqlUpdate("DELETE FROM tbl_data_sensor WHERE id_sensor=:id")
   boolean delete(long id);
}

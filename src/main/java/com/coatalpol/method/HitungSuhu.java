/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coatalpol.method;
import java.math.*;
/**
 *
 * @author IDEAPAD 110
 */
public class HitungSuhu {
    private double suhu, la, lb, lrata2,
                   ci, suhubaru;

    public HitungSuhu(double suhu) {
        this.suhu = suhu;
        this.la = 28;
        this.lb = 30;
        
        lrata2 = (la+lb)/2;
        if (suhu <= lrata2) {
            ci = (suhu - lrata2)/(la-lrata2);
        }
        else {
            ci = (suhu - lrata2)/(lb-lrata2);
        }
        
        if (ci < 1) {
            suhubaru = ci;
        }
        else {
            suhubaru = 1 + (5*(Math.log(ci)));
        }
    }
    
    public double getSuhuBaru() {
        return suhubaru;
    }
}

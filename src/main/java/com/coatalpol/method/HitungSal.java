/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coatalpol.method;
import java.math.*;
/**
 *
 * @author IDEAPAD 110
 */
public class HitungSal {
    private double sal, la, lb, lrata2,
                   ci, salbaru;

    public HitungSal(double sal) {
        this.sal = sal;
        this.la = 33;
        this.lb = 34;
        
        lrata2 = (la+lb)/2;
        if (sal <= lrata2){
            ci = (sal - lrata2)/(la - lrata2);
        }
        else {
            ci = (sal - lrata2)/(lb - lrata2);
        }
     
        if (ci < 1) {
            salbaru = ci;
        }
        else {
            salbaru = 1 + (5*(Math.log(ci)));
        }
    }

    public double getSalBaru() {
        return salbaru;
    }
}

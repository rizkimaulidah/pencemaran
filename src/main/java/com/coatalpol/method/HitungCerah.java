/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coatalpol.method;
import java.math.*;
/**
 *
 * @author IDEAPAD 110
 */
public class HitungCerah {
    private double cerah, l, cmaks
                   , ci, cbaru;
    
    public HitungCerah(double cerah){
        this.cerah = cerah;
        this.l = 6;
        this.cmaks = 12;
        
        cbaru = (cmaks - cerah)/(cmaks-l);
        ci = cbaru / l;
        
        if (ci < 1){
            cbaru = ci;
        }
        else {
            cbaru = 1 + (5*(Math.log(ci)));
        }
    }

    public double getCerahBaru() {
        return cbaru;
    }
}

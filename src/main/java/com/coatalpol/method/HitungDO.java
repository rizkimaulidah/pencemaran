/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coatalpol.method;
import java.math.*;

/**
 *
 * @author IDEAPAD 110
 */
public class HitungDO {
    private double disox, l, domaks,
                   ci, dobaru;

    public HitungDO(double disox) {
        this.disox = disox;
        this.l = 6;
        this.domaks = 7;
        
        dobaru = (domaks - disox)/(domaks-l);
        ci = dobaru/l;
        // System.out.println("Cdo = (" + domaks + "-" + disox + "/" + domaks + "-" + l);
        
        
        if (ci < 1){
            dobaru = ci;
        }
        else {
            dobaru = 1 + (5*(Math.log(ci)));
        }
    }

    public double getDoBaru() {
        return dobaru;
    }
}

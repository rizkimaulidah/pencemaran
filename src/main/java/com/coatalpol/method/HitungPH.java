/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.coatalpol.method;
import java.math.*;
/**
 *
 * @author IDEAPAD 110
 */
public class HitungPH {
    private double ph, la, lb, lrata2, 
                   ci, phbaru;
    
    //Constructor
    public HitungPH(double ph) {
        this.ph = ph;
        this.la = 7;
        this.lb = 8.5;
        
        lrata2 = (la+lb)/2;

        if(ph <= lrata2){
            ci = (ph - lrata2)/(la - lrata2);
        }
        else {
            ci = (ph - lrata2)/(lb - lrata2);
        }
        if (ci < 1){
            phbaru = ci;
        }
        else {
            phbaru = 1 + (5*(Math.log(ci)));
        }
    }
    
    public double getPhBaru(){
        return phbaru;
    }
}

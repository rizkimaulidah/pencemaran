/*
 * Copyright 2018 IDEAPAD 110.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.coatalpol.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author IDEAPAD 110
 */
public class Admin {
    public static class Mapper implements RowMapper<Admin> {

        @Override
        public Admin map(final ResultSet rs, final StatementContext ctx) throws SQLException {
            return new Admin(rs.getInt("id_admin"), rs.getString("username"), rs.getString("passwd"), rs.getInt("role_admin"));
        }
    }
    
    private int idAdmin;
    private String username;
    private String passwd;
    private int roleAdmin;
    
    public Admin(int idAdmin, String username, String passwd, int roleAdmin){
        this.idAdmin = idAdmin;
        this.username = username;
        this.passwd = passwd;
        this.roleAdmin = roleAdmin;
    }

    public int getIdAdmin() {
        return idAdmin;
    }

    public String getUsername() {
        return username;
    }

    public String getPasswd() {
        return passwd;
    }

    public int getRoleAdmin() {
        return roleAdmin;
    }
    
    
}

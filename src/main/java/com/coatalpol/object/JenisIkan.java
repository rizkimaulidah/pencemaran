package com.coatalpol.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JenisIkan {
	public static class Mapper implements RowMapper<JenisIkan> {
		@Override public JenisIkan map(final ResultSet rs, final StatementContext ctx) throws SQLException {
	      	return new JenisIkan(rs.getInt("id_jenis"), rs.getString("jenis_ikan"));
	    }
	}

	private int idJenis;
	private String jenisIkan;

	public JenisIkan(int id_jenis, String jenis_ikan) {
		this.idJenis = id_jenis;
		this.jenisIkan = jenis_ikan;
	}

	public int getIdJenis() {
		return idJenis;
	}

	public String getJenisIkan() {
		return jenisIkan;
	}
}

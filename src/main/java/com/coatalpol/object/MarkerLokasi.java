package com.coatalpol.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MarkerLokasi {

  public static class Mapper implements RowMapper<MarkerLokasi> {
    @Override public MarkerLokasi map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new MarkerLokasi(rs.getInt("gid"), rs.getString("st_AsText"), rs.getString("name"), rs.getString("latitude")
                            , rs.getString("longitude"), rs.getString("lokasi"));
    }
  }

  private int gid;
  private String geom;
  private String nama;
  private String lat;
  private String lan;
  private String lokasi;

  public MarkerLokasi(int gid, String the_geom, String name, String latitude, String longitude, String lokasi) {
    this.gid = gid;
    this.geom = the_geom;
    this.nama = name;
    this.lat = latitude;
    this.lan = longitude;
    this.lokasi = lokasi;
  }

  public int getId() {
    return gid;
  }

  public String getGeom() {
    return geom;
  }
  
  public String getNama() {
    return nama;
  }

  public String getLokasi() {
    return lokasi;
  }


  // public String getLat() {
  //   return parseGeom(geom, "lat");
  // }

  // public String getLan() {
  //   return parseGeom(geom, "lan");
  // }
  //  public String parseGeom(String geom, String getWhat) {
  //     geom = geom.replace("POINT(", "").replace(")", "");
  //     String koordinat[] = geom.split(" ");
  //     String toReturn = "";
      
  //     if(getWhat == "lat") {
  //         toReturn = koordinat[1];
  //     } else {
  //         toReturn = koordinat[0];
  //     }
      
  //     return toReturn;
  // }
}

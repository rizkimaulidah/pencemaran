package com.coatalpol.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
/**
 *
 * @author IDEAPAD 110
 */
public class DataSensor {

    public static class Mapper implements RowMapper<DataSensor> {

        @Override
        public DataSensor map(final ResultSet rs, final StatementContext ctx) throws SQLException {
            return new DataSensor(rs.getInt("id_sensor"), rs.getString("nilai_ph"), rs.getString("nilai_do")
                                , rs.getString("nilai_cerah"), rs.getString("nilai_sal"), rs.getString("nilai_suhu")
                                , rs.getString("wkt_input"), rs.getString("nama"), rs.getString("lokasi"), rs.getString("st_AsText"));
        }
    }
    
    private int idSensor;
    private String nilaiPh;
    private String nilaiDo;
    private String nilaiCerah;
    private String nilaiSal;
    private String nilaiSuhu;
    private String wktInput;
    private String nama;
    private String lokasi;
    private String geom;
    
    public DataSensor(int idSensor, String nilaiPh, String nilaiDo, String nilaiCerah, String nilaiSal, String nilaiSuhu
                    , String wktInput, String nama, String lokasi, String geom){
        this.idSensor = idSensor;
        this.nilaiPh = nilaiPh;
        this.nilaiDo = nilaiDo;
        this.nilaiCerah = nilaiCerah;
        this.nilaiSal = nilaiSal;
        this.nilaiSuhu = nilaiSuhu;
        this.wktInput = wktInput;
        this.nama = nama;
        this.lokasi = lokasi;
        this.geom = geom;
    }

    public int getIdSensor() {
        return idSensor;
    }

    public String getNilaiPh() {
        return nilaiPh;
    }

    public String getNilaiDo() {
        return nilaiDo;
    }

    public String getNilaiCerah() {
        return nilaiCerah;
    }

    public String getNilaiSal() {
        return nilaiSal;
    }

    public String getNilaiSuhu() {
        return nilaiSuhu;
    }

    public String getWktInput() {
        return wktInput;
    }

    public String getNama() {
        return nama;
    }

    public String getLokasi() {
        return lokasi;
    }
    
    public String getGeom() {
        return geom;
    }

    public void setWktInput(String wktInput) {
        this.wktInput = wktInput;
    }
}

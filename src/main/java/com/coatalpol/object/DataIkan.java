package com.coatalpol.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DataIkan {
	public static class Mapper implements RowMapper<DataIkan> {
		@Override public DataIkan map(final ResultSet rs, final StatementContext ctx) throws SQLException {
	      	return new DataIkan(rs.getInt("id_ikan"), rs.getString("jenis_ikan"), rs.getString("jml_ikan_sehat")
	      						, rs.getString("jml_ikan_sakit"), rs.getString("wkt_input"));
	    }
	}

	private int idIkan;
	private String jenisIkan;
	private String jmlIkanSehat;
	private String jmlIkanSakit;
	private String wktInput;

	public DataIkan(int id_ikan, String jenis_ikan, String jml_ikan_sehat, String jml_ikan_sakit, String wkt_input) {
		this.idIkan = id_ikan;
		this.jenisIkan = jenis_ikan;
		this.jmlIkanSehat = jml_ikan_sehat;
		this.jmlIkanSakit = jml_ikan_sakit;
		this.wktInput = wkt_input;
	}

	public int getIdIkan() {
		return idIkan;
	}

	public String getJenisIkan() {
		return jenisIkan;
	}

	public String getJmlIkanSehat() {
		return jmlIkanSehat;
	}

	public String getJmlIkanSakit() {
		return jmlIkanSakit;
	}

	public String getWktInput() {
        return wktInput;
    }

    public void setWktInput(String wktInput) {
    	this.wktInput = wktInput;
  	}
}

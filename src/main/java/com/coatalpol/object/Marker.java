package com.coatalpol.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Marker {

  public static class Mapper implements RowMapper<Marker> {
    @Override public Marker map(final ResultSet rs, final StatementContext ctx) throws SQLException {
      return new Marker(rs.getInt("gid"), rs.getString("name"), rs.getString("st_astext"));
    }
  }

  private int gid;
  private String name;
  private String lokasi;

  public Marker(int gid, String name, String lokasi) {
    this.gid = gid;
    this.name = name;
    this.lokasi = lokasi;
  }

  public int getGid() {
    return gid;
  }

  public String getName() {
    return name;
  }

  public String getLokasi() {
    return lokasi;
  }
}

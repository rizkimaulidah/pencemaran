package com.coatalpol.object;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HitungMetode {
	public static class Mapper implements RowMapper<HitungMetode> {
		@Override public HitungMetode map(final ResultSet rs, final StatementContext ctx) throws SQLException {
			return new HitungMetode(rs.getInt("id"), rs.getInt("id_data_sensor"), rs.getDouble("indeks"), rs.getString("status"), rs.getString("waktu_input"));
		}
	}

	private int id;
	private int idDataSensor;
	private double indeks;
	private String status;
	private String waktuInput;

	public HitungMetode(int id, int idDataSensor, double indeks, String status, String waktuInput) {
		this.id = id;
		this.idDataSensor = idDataSensor;
		this.indeks = indeks;
		this.status = status;
		this.waktuInput = waktuInput;
	}

	public int getId() {
		return id;
	}

	public int getIdDataSensor() {
		return idDataSensor;
	}

	public double getIndeks() {
		return indeks;
	}

	public String getStatus() {
		return status;
	}

	public String getWaktuInput() {
		return waktuInput;
	}

    public void setWaktuInput(String waktuInput) {
        this.waktuInput = waktuInput;
    }
}
package com.coatalpol;

import com.coatalpol.api.*;
import com.coatalpol.repository.*;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.jooby.Err;
import org.jooby.Jooby;
import org.jooby.Results;
import org.jooby.Status;
import org.jooby.jdbc.Jdbc;
import org.jooby.jdbi.Jdbi3;
import org.jooby.jdbi.TransactionalRequest;
import org.jooby.json.Jackson;
import org.jooby.apitool.ApiTool;
import org.jooby.Session;
import org.jooby.hbs.Hbs;

/**
 * @author jooby generator
 */
public class App extends Jooby {
  	{
        port( 8181 );
    	get("/home", () -> {return Results.html("home");});
    	get("/add", () -> {return Results.html("TambahForm");});


    	get("/adminPage", req -> {
    		Session session = req.session();

    		if(session.isSet("nama")) {
    			req.set("session", session);
    			return Results.html("admin/overview");
    			//render hal admin
    		} else {
    			return Results.redirect("/loginPage");
    		}
    	});

    	get("/loginPage", req -> {
    		Session session = req.session();

    		if(session.isSet("nama")) {
    			req.set("session", session);
    			return Results.redirect("/adminPage");
    			// redirect hal admin
    		} else {
    			return Results.html("LoginForm");
    			//return "hehe";
    		}
    	});

  	}

  	{
	    use(new Jackson());
	    use(new Jdbc());
	    use(new Hbs());
	    use(new Jdbi3()
	        /** Install SqlObjectPlugin */
	        .doWith(jdbi -> {
	          jdbi.installPlugin(new SqlObjectPlugin());
	        })
	        /** Creates a transaction per request and attach Repository */
	        .transactionPerRequest(
	            new TransactionalRequest()
                    .attach(DataIkanRepo.class)
                    .attach(DataSensorRepo.class)
                    .attach(AdminRepo.class)
                    .attach(JenisIkanRepo.class)
                    .attach(HitungMetodeRepo.class)
	        )
	    );

	    assets("/**");
        assets("/assets/**");

        //~ Halaman
        assets("/overview", "admin/overview.html");
        assets("/data-sensor", "admin/data-sensor.html");
        assets("/history", "admin/history.html");
        assets("/statistik", "admin/statistik.html");
        assets("/data-ikan", "admin/data-ikan.html");
        assets("/admin", "AdminPage.html");
        assets("/index", "view/index.html");
        assets("/nama-ikan", "view/data-ikan.html");
        assets("/view", "view/view.html");
        assets("/bantuan", "bantuan.html");

        //~ API()
		use(new ProsesLogin());
		use(new MarkerA());
		use(new AdminA());
		use(new DataIkanA());
		use(new DataSensorA());
		use(new JenisIkanA());
        use(new HitungMetodeA());

            
	    /** Export API to Swagger and RAML: */
   	  use(new ApiTool()
        .swagger("/swagger")
      );
	}

  	public static void main(final String[] args) {
    	run(App::new, args);
  	}

}

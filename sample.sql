--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: tbl_data_ikan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tbl_data_ikan (
    id_ikan integer NOT NULL,
    jenis_ikan character varying(50) NOT NULL,
    jml_ikan_sehat character varying,
    jml_ikan_sakit character varying,
    wkt_input timestamp without time zone
);


ALTER TABLE tbl_data_ikan OWNER TO postgres;

--
-- Name: data_ikan_id_ikan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE data_ikan_id_ikan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE data_ikan_id_ikan_seq OWNER TO postgres;

--
-- Name: data_ikan_id_ikan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE data_ikan_id_ikan_seq OWNED BY tbl_data_ikan.id_ikan;


--
-- Name: tbl_admin; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tbl_admin (
    id_admin integer NOT NULL,
    username character varying(50) NOT NULL,
    passwd character varying(50) NOT NULL,
    role_admin integer
);


ALTER TABLE tbl_admin OWNER TO postgres;

--
-- Name: tbl_admin_id_admin_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_admin_id_admin_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_admin_id_admin_seq OWNER TO postgres;

--
-- Name: tbl_admin_id_admin_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_admin_id_admin_seq OWNED BY tbl_admin.id_admin;


--
-- Name: tbl_data_sensor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tbl_data_sensor (
    id_sensor integer NOT NULL,
    nilai_ph character varying(25) NOT NULL,
    nilai_do character varying(25) NOT NULL,
    nilai_cerah character varying(25) NOT NULL,
    nilai_sal character varying(25) NOT NULL,
    nilai_suhu character varying(25) NOT NULL,
    wkt_input timestamp without time zone,
    nama character varying,
    lokasi character varying,
    geom geometry(Point,4326)
);


ALTER TABLE tbl_data_sensor OWNER TO postgres;

--
-- Name: tbl_data_sensor_id_sensor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_data_sensor_id_sensor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_data_sensor_id_sensor_seq OWNER TO postgres;

--
-- Name: tbl_data_sensor_id_sensor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_data_sensor_id_sensor_seq OWNED BY tbl_data_sensor.id_sensor;


--
-- Name: tbl_jenis_ikan; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tbl_jenis_ikan (
    id_jenis integer NOT NULL,
    jenis_ikan character varying NOT NULL
);


ALTER TABLE tbl_jenis_ikan OWNER TO postgres;

--
-- Name: tbl_jenis_ikan_id_jenis_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_jenis_ikan_id_jenis_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_jenis_ikan_id_jenis_seq OWNER TO postgres;

--
-- Name: tbl_jenis_ikan_id_jenis_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_jenis_ikan_id_jenis_seq OWNED BY tbl_jenis_ikan.id_jenis;


--
-- Name: tbl_parameter; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tbl_parameter (
    id integer NOT NULL,
    id_data_sensor integer,
    indeks numeric,
    status character varying,
    waktu_input timestamp without time zone
);


ALTER TABLE tbl_parameter OWNER TO postgres;

--
-- Name: tbl_parameter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_parameter_id_seq OWNER TO postgres;

--
-- Name: tbl_parameter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_parameter_id_seq OWNED BY tbl_parameter.id;


--
-- Name: id_admin; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_admin ALTER COLUMN id_admin SET DEFAULT nextval('tbl_admin_id_admin_seq'::regclass);


--
-- Name: id_ikan; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_data_ikan ALTER COLUMN id_ikan SET DEFAULT nextval('data_ikan_id_ikan_seq'::regclass);


--
-- Name: id_sensor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_data_sensor ALTER COLUMN id_sensor SET DEFAULT nextval('tbl_data_sensor_id_sensor_seq'::regclass);


--
-- Name: id_jenis; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_jenis_ikan ALTER COLUMN id_jenis SET DEFAULT nextval('tbl_jenis_ikan_id_jenis_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_parameter ALTER COLUMN id SET DEFAULT nextval('tbl_parameter_id_seq'::regclass);


--
-- Name: data_ikan_id_ikan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('data_ikan_id_ikan_seq', 45, true);


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY spatial_ref_sys  FROM stdin;
\.


--
-- Data for Name: tbl_admin; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tbl_admin (id_admin, username, passwd, role_admin) FROM stdin;
\.


--
-- Name: tbl_admin_id_admin_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_admin_id_admin_seq', 1, false);


--
-- Data for Name: tbl_data_ikan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tbl_data_ikan (id_ikan, jenis_ikan, jml_ikan_sehat, jml_ikan_sakit, wkt_input) FROM stdin;
34	Tongkol komo	12	0.1	2018-07-27 11:31:00
35	Cakalang	17	0.02	2018-07-10 01:01:00
37	Kenyar	18	0.1	2018-07-03 13:09:00
39	Slengseng	23	0.01	2018-07-02 01:32:00
40	Kembung	10	0.002	2018-07-04 16:02:00
41	Tenggiri	40	1	2018-07-11 18:33:00
42	Kerapu karang	90	8	2018-07-18 16:44:00
43	Kenyar	20	0.4	2018-07-25 13:03:00
44	Slengseng	17	0.3	2018-07-26 14:33:00
45	Tuna sirip	57	3	2018-07-13 14:22:00
\.


--
-- Data for Name: tbl_data_sensor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tbl_data_sensor (id_sensor, nilai_ph, nilai_do, nilai_cerah, nilai_sal, nilai_suhu, wkt_input, nama, lokasi, geom) FROM stdin;
20	8.42	6.18	4.7	27.8	4.7	2018-04-07 15:01:00	A2	Citemu	0101000020E6100000D9F4DC9113285B40405B43F879081BC0
21	8.42	5.99	7.2	28.9	30.0	2018-04-07 15:01:00	A3	Mundu	0101000020E61000002AA8593E56295B408BD9F4DC91F31AC0
22	8.41	5.82	4.59	28.1	29.9	2018-07-07 14:03:00	A4	Kejawanan	0101000020E6100000BC4EAEDAD3295B40E8218F2F03EA1AC0
19	8.40	5.57	4.30	28.8	29.8	2018-03-08 04:01:00	A5	Kejawanan	0101000020E610000005E9004CCA2B5B4043F879C8E3EB1AC0
23	8.35	5.65	8.10	28.0	29.9	2018-04-08 13:03:00	A6	Citemu	0101000020E610000030701725992B5B400BFD88AE6D061BC0
41	8.28	5.80	16.20	28.1	29.9	2018-07-10 12:59:00	A7	Losari	0101000020E6100000A3B765AD8E2B5B406078C341F3171BC0
42	8.34	5.71	5.50	29.0	29.9	2018-07-21 12:59:00	A8	Pangenan	0101000020E6100000D7A3703D0A2D5B405AD148C0372F1BC0
3	8.28	5.88	14.40	27.5	30.0	2018-02-01 01:07:00	A9	Pangenan (muara sungai)	0101000020E6100000FD62C92F962E5B405D6E7F90A1321BC0
4	8.26	5.61	33.60	28.0	29.5	2018-02-01 01:07:00	A10	TPI Gebang	0101000020E61000000CB6600BB62E5B40C011CAE86D391BC0
5	8.26	5.60	26.70	28.0	29.8	2018-02-01 01:07:00	A11	Kesunean	0101000020E610000025BF58F28B255B4057EDE94C16E61AC0
6	8.24	5.36	32.50	28.8	29.9	2018-02-01 01:07:00	A12	Ade irma	0101000020E6100000E0C13C51FF245B407B14AE47E1DA1AC0
7	8.20	5.48	58.30	28.1	30.0	2018-02-01 01:07:00	A13	Panjunan	0101000020E6100000A575DFE27F245B40D42C1FABD0CF1AC0
8	8.16	5.09	77.60	27.4	29.9	2018-02-01 01:07:00	A14	Pelabuhan Cirebon (muara sungai)	0101000020E6100000B4A291806F245B40029894F7C0D01AC0
9	8.52	7.87	52.90	25.2	30.2	2018-02-01 01:07:00	A15	Samadikun	0101000020E610000066AD8E091E245B407242ACAF4CC31AC0
44	7.87	3.54	154.32	27.5	29.8	2018-07-24 00:25:00	A16	Tangkil	0101000020E61000004444444444245B406CC1166CC1B61AC0
39	8.36	6.28	2.9	27.4	30.1	2018-04-08 14:00:00	A1	Cangkol	0101000020E6100000BA490C022B275B40A166F95885DE1AC0
47	8.23	5.42	83.20	27.8	30.1	2018-07-24 00:52:00	A17	Klayan	0101000020E6100000C6FF6C0DE1235B40B6A7335918B11AC0
48	8.16	5.42	83.20	27.8	30.1	2018-07-24 00:57:00	A18	Gunung jati	0101000020E6100000CA7B6078C3235B40A596224807A01AC0
49	8.32	5.74	50.30	26.8	30.1	2018-07-24 01:02:00	A19	TPI Bondet	0101000020E61000006ABC749318245B4020F79A0B49931AC0
50	8.45	6.39	57.10	27.7	30.0	2018-07-24 01:08:00	A20	Bungko	0101000020E610000087F0F390C7235B4009AC1C5A647B1AC0
\.


--
-- Name: tbl_data_sensor_id_sensor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_data_sensor_id_sensor_seq', 53, true);


--
-- Data for Name: tbl_jenis_ikan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tbl_jenis_ikan (id_jenis, jenis_ikan) FROM stdin;
1	Tongkol komo
2	Cakalang
3	Kembung
4	Banyar
5	Kenyar
6	Slengseng
7	Tenggiri
8	Tenggiri papan
9	Tuna sirip
10	Kerapu karang
\.


--
-- Name: tbl_jenis_ikan_id_jenis_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_jenis_ikan_id_jenis_seq', 10, true);


--
-- Data for Name: tbl_parameter; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tbl_parameter (id, id_data_sensor, indeks, status, waktu_input) FROM stdin;
196	39	9.81280430897738	TS	2018-07-25 02:46:00.56
197	20	10.3009775040424	TT	2018-07-25 02:47:00.13
198	21	8.78450315467748	TS	2018-07-25 02:47:00.18
200	39	9.81280430897738	TS	2018-08-06 22:49:00.11
201	20	10.3009775040424	TT	2018-08-06 22:54:00.08
202	21	8.78450315467748	TS	2018-08-06 22:58:00.24
203	22	9.35962916128909	TS	2018-08-06 22:59:00.59
204	19	8.85698564673787	TS	2018-08-06 23:02:00.21
205	23	9.42412094588376	TS	2018-08-06 23:04:00.19
206	41	9.3542529676109	TS	2018-08-06 23:06:00.5
207	42	8.7001625467202	TS	2018-08-06 23:08:00.15
208	3	9.73672456728338	TS	2018-08-06 23:09:00.52
209	4	9.40813334914158	TS	2018-08-06 23:11:00.39
210	5	9.41746342355525	TS	2018-08-06 23:13:00.39
211	6	8.85449356569687	TS	2018-08-06 23:14:00.31
212	7	9.35571897113686	TS	2018-08-06 23:15:00.39
213	8	9.79227485385394	TS	2018-08-06 23:17:00.23
214	9	10.9384278745432	TT	2018-08-06 23:18:00.31
215	44	2.15517069885578	TR	2018-08-06 23:19:00.27
216	47	9.56742893645014	TS	2018-08-06 23:21:00.22
217	48	9.5644034529524	TS	2018-08-06 23:22:00.1
218	49	10.1520634521163	TT	2018-08-06 23:25:00.44
219	50	9.61901709285749	TS	2018-08-06 23:27:00.05
175	22	11.9437908565683	TT	2018-07-24 00:01:00.05
176	19	15.8613348220384	TT	2018-07-24 00:05:00.37
177	23	16.1881453619634	TT	2018-07-24 00:07:00.36
178	41	8.58589705570486	TS	2018-07-24 00:09:00.17
179	42	17.0477655714349	TT	2018-07-24 00:10:00.48
180	3	7.70619725641433	TS	2018-07-24 00:13:00.04
181	4	2.76507923521433	TR	2018-07-24 00:15:00.06
182	5	4.83995243614663	TR	2018-07-24 00:16:00.31
183	6	10.041110753999	TT	2018-07-24 00:18:00.1
184	7	13.3463735302696	TT	2018-07-24 00:19:00.46
185	8	14.0024022699848	TT	2018-07-24 00:21:00.22
186	9	9.41164299714782	TS	2018-07-24 00:23:00.28
187	44	2.15517069885578	TR	2018-07-24 00:25:00.4
190	47	9.56742893645014	TS	2018-07-24 00:52:00.26
191	48	9.5644034529524	TS	2018-07-24 00:57:00.09
192	49	10.1520634521163	TT	2018-07-24 01:02:00.53
193	50	9.61901709285749	TS	2018-07-24 01:08:00.2
\.


--
-- Name: tbl_parameter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_parameter_id_seq', 219, true);


--
-- Name: data_ikan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tbl_data_ikan
    ADD CONSTRAINT data_ikan_pkey PRIMARY KEY (id_ikan);


--
-- Name: tbl_admin_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tbl_admin
    ADD CONSTRAINT tbl_admin_pkey PRIMARY KEY (id_admin);


--
-- Name: tbl_data_sensor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tbl_data_sensor
    ADD CONSTRAINT tbl_data_sensor_pkey PRIMARY KEY (id_sensor);


--
-- Name: tbl_jenis_ikan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tbl_jenis_ikan
    ADD CONSTRAINT tbl_jenis_ikan_pkey PRIMARY KEY (id_jenis);


--
-- Name: tbl_parameter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tbl_parameter
    ADD CONSTRAINT tbl_parameter_pkey PRIMARY KEY (id);


--
-- Name: tbl_parameter_id_data_sensor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_parameter
    ADD CONSTRAINT tbl_parameter_id_data_sensor_fkey FOREIGN KEY (id_data_sensor) REFERENCES tbl_data_sensor(id_sensor) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

